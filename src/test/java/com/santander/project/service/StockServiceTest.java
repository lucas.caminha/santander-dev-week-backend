package com.santander.project.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.santander.project.exceptions.BusinessException;
import com.santander.project.model.dto.StockDTO;

@SpringBootTest
@SuppressWarnings("deprecation")
class StockServiceTest {

	@Autowired
	private StockService service;
	
	@Test
	void deveLancarBusinessExceptionAoSalvarStockExistente() {		
		StockDTO stockDTO = new StockDTO(null, "Ponto Frio", 120.2, LocalDate.of(2021, 05, 28), 0.43);
		assertThrows(BusinessException.class, () -> service.save(stockDTO));			
	}
	
	/**
	@Test
	void deveRetornarUmStockDTOAoSalvarNovoStock() {
		StockDTO stockDTO = new StockDTO(null, "Atakarejo", 25.2, LocalDate.of(2021, 12, 04), 0.13);
		assertEquals(StockDTO.class, service.save(stockDTO).getClass());
	}
	**/
	@Test
	void deveLancarBusinessExceptionAoAtualizarStockComDadosIguais() {
		StockDTO stockDTO = new StockDTO(new Long("1"), "Ponto Frio", 120.2, LocalDate.of(2021, 05, 28), 0.43);
		assertThrows(BusinessException.class, () -> service.update(stockDTO));
	}

	@Test
	void deveRetornarUmStockDTOAoAtualizarNovoStock() {
		StockDTO stockDTO = new StockDTO(new Long(2), "Nubank", 25.2, LocalDate.of(2021, 12, 07), 0.26);
		assertEquals(StockDTO.class, service.update(stockDTO).getClass());
	}

}
