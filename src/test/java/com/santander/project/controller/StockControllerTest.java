package com.santander.project.controller;

import com.santander.project.model.dto.StockDTO;
import com.santander.project.repository.StockRepository;
import com.santander.project.service.StockService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
//@RunWith(MockitoJUnitRunner.class)
public class StockControllerTest {

    @Autowired
    private StockController stockController;

    @Test
    public void deveRetornarRespostaDeSucessoAoSalvarUmStockDto() {
        StockDTO stockDTO = new StockDTO(null, "Atakarejo", 25.2,
                LocalDate.of(2021, 12, 04), 0.13);
        StockService mockService = Mockito.spy(new StockService());
        StockDTO stockWithId = new StockDTO(29L, "Atakarejo", 25.2,
                LocalDate.of(2021, 12, 04), 0.13);
        Mockito.doReturn(stockWithId).when(mockService).save(stockDTO);
        ResponseEntity finalResponse = ResponseEntity.ok(stockWithId);
        ResponseEntity controllerResponse = stockController.save(stockDTO);

        Assertions.assertEquals(finalResponse.getClass(), controllerResponse.getClass());
        Assertions.assertEquals(finalResponse.getStatusCode(), controllerResponse.getStatusCode());
    }
}
